var vm = new Vue({
      el: '.container-fulid',
      data: {
        user: '',
        tpl_panels_shows: true, //活动面板
        his_panel_shows: false, //历史面板
        tpl_info_shows: false, //活动信息面板
        his_info_shows: false, //历史活动信息
        tpls: [],
        his: [],
        cur_tpl: {},
        cur_his_item: {},
        datas: {
          input_data: {},
          user: '',
          date: ''
        }
      },
      ready: function() {
        this.$http.get('/users/get_info').then(function(data) {
          vm.tpls = data.body.tpls;
          vm.user = data.body.user;
          vm.his = data.body.his;
        })
      },
      methods: {
        show_panels: function(id) {
          if (id == 0) {
            vm.tpl_panels_shows = true;
            vm.tpl_info_shows = false;
            vm.his_panel_shows = false;
            vm.his_info_shows = false;
            this.$http.get('/users/get_info').then(function(data) {
                vm.tpls = data.body.tpls;
                vm.user = data.body.user;
                vm.his = data.body.his;
              });
            }
            else if (id == 1) {
              vm.tpl_panels_shows = false;
              vm.tpl_info_shows = false;
              vm.his_panel_shows = true;
              vm.his_info_shows = false;
            } else if (id == 2) {
              vm.tpl_panels_shows = false;
              vm.tpl_info_shows = false;
              vm.his_panel_shows = false;
              vm.his_info_shows = true;
            }
          },
          get_tpl: function(name) {
              vm.tpl_info_shows = true;
              vm.tpl_panels_shows = false;
              vm.his_panel_shows = false;
              vm.tpls.forEach(function(el, index) {
                if (el.name == name)
                  vm.cur_tpl = el;
              });
            },
            submit_info: function() {
              console.log(vm.datas);
              vm.datas.user = vm.user;
              vm.datas.date = new Date().toLocaleString();
              var datas = {
                tpl_name: vm.cur_tpl.name,
                data: vm.datas
              };
              this.$http.post('/users/sub_info', datas).then(function(data) {
                if (data.body == 'success') {
                  vm.show_panels(0);
                }
              });

            },
            get_his_info: function(name) {
              vm.his.forEach(function(el, index) {
                if (el.name == name) {
                  vm.cur_his_item = el;
                }
              });
              vm.tpls.forEach(function(el, index) {
                if (el.name == name) {
                  vm.cur_tpl = el;
                }
              });
              vm.show_panels(2);
            }
        }
      })
