var vm = new Vue({
  el: '.container-fulid',
  data: {
    user: '',
    remsg: '',
    users: [],
    tpls: [],
    logs: [],
    users_shows: false,
    tpls_shows: true,
    logs_shows: false,
    addtpl_shows: false,
    adduser_shows: false,
    curtpl_info_shows: false,
    tpl: {}, //用于提交/更新的tpl对象
    cur_tpl: {}, //用于查看tpl信息的tpl对象
    inputs: []
      // cur_panel: 0,
      // panels: [false, false, false],
  },
  ready: function() {
    this.$http.get('/admin/get_info').then(function(data) {
        vm.tpls = data.body.tpls;
        vm.users = data.body.comusers;
        vm.logs = data.body.logs;
        vm.user = data.body.user;
      })
      // console.log(vm.tpls,vm.user);
  },
  methods: {
    get_info: function() {
      this.$http.get('/admin/get_info').then(function(data) {
          vm.tpls = data.body.tpls;
          vm.users = data.body.comusers;
          vm.logs = data.body.logs;
          vm.user = data.body.user;
        })
        // console.log(vm.tpls,vm.user);
    },
    show_panels: function(id) {
      if (id == 0) {
        vm.tpls_shows = true;
        vm.adduser_shows = vm.addtpl_shows = vm.users_shows = vm.logs_shows = false;
      } else if (id == 1) {
        vm.users_shows = true;
        vm.adduser_shows = vm.addtpl_shows = vm.tpls_shows = vm.logs_shows = false;
      } else if (id == 2) {
        vm.logs_shows = true;
        vm.adduser_shows = vm.addtpl_shows = vm.users_shows = vm.tpls_shows = false;
      } else if (id == 3) {
        vm.addtpl_shows = true;
        vm.adduser_shows = vm.logs_shows = vm.users_shows = vm.tpls_shows = false;
      } else if (id == 4) {
        vm.adduser_shows = true;
        vm.addtpl_shows = vm.logs_shows = vm.users_shows = vm.tpls_shows = false;
      }
      vm.curtpl_info_shows = false;
    },
    show_curtpl_info: function(name) {
      vm.curtpl_info_shows = true;
      vm.tpls_shows = false;
      vm.tpls.forEach(function(el, index) {
        if (el.name == name)
          vm.cur_tpl = el;
      });
    },
    add_user: function() {
      this.$http.post('admin/add_user', vm.new_user).then(function(data) {
        if (data.body == 'success') {
          vm.add_log('添加用户' + vm.new_user.username);
          vm.get_info();
          vm.show_panels(1);
        }
      })
    },
    dele_user: function(name) {
      var user_con = { username: name };
      this.$http.post('admin/dele_user', user_con).then(function(data) {
        if (data.body == 'success') {
          vm.get_info();
          // vm.show_panels(0);
          vm.add_log('删除用户' + name);
        } else {
          vm.remsg = data.body
        }
      }).catch(function(e) { console.error(e) });
    },
    add_input: function(type) {
      var input = {
        index: vm.inputs.length,
        name: new Date().getTime(),
        type: type,
        label: '',
        des: ''
      };
      if (type == 'radio' || type == 'checkbox') {
        input.opts = [{
          name: new Date().getTime(),
          des: '',
          check: true
        }]
      }
      vm.inputs.push(input);
    },
    dele_input: function(name) {
      console.log(name);
      vm.inputs.splice(vm.inputs.findIndex(function(el) {
        return el.name == name;
      }), 1);
    },
    add_opt: function(name) {
      console.log(name);
      vm.inputs[vm.inputs.findIndex(function(el) {
        return el.name == name;
      })].opts.push({
        name: new Date().getTime(),
        des: '',
        check: false
      })
    },
    add_tpl: function() {
      vm.tpl.inputs = vm.inputs;
      vm.tpl.count = vm.tpl.count || 0;
      vm.tpl.datas = vm.tpl.datas || [];
      vm.tpl.clue = vm.tpl.clue || [];
      console.log(vm.tpl);
      this.$http.post('admin/add_tpl', vm.tpl)
        .then(function(data) {
          console.log(data);
          if (data.body == 'success') {
            vm.add_log('添加/修改活动' + vm.tpl.name);
            vm.inputs = [];
            vm.tpl = {};
            vm.get_info();
            vm.show_panels(0);
          } else {
            vm.remsg = data.body;
          }
        })
        .catch(function(e) { console.error(e) });
    },
    dele_tpl: function(name) {
      var tpl_con = { name: name };
      this.$http.post('admin/dele_tpl', tpl_con).then(function(data) {
        if (data.body == 'success') {
          vm.get_info();
          // vm.show_panels(0);
          vm.add_log('删除活动' + name);
        } else {
          vm.remsg = data.body
        }
      }).catch(function(e) { console.error(e) });
    },
    update_tpl: function(name) {
      var cur_tpl = {};
      vm.tpls.forEach(function(el, index) {
        if (el.name == name)
          cur_tpl = el;
      })
      console.log(cur_tpl);
      vm.inputs = cur_tpl.inputs;
      vm.tpl = cur_tpl;
      vm.show_panels(3);
    },
    add_log: function(des) {
      var log = {
        name: new Date().getTime(),
        date: new Date().toLocaleString(),
        des: des,
        user: vm.user
      };
      this.$http.post('admin/add_log', log).then(function(data) {
        if (data.body == 'success') {
          vm.get_info();
        } else
          vm.remsg = data.body;
      }).catch(function(e) { console.error(e) });
    },
    dele_log: function(name) {
      var log_con = { name: name };
      this.$http.post('admin/dele_log', log_con).then(function(data) {
        if (data.body == 'success') {
          vm.get_info();
          // vm.show_panels(0);
        } else {
          vm.remsg = data.body
        }
      }).catch(function(e) { console.error(e) });
    }
  }
})
