var vm = new Vue({
  el:'.container-fluid',
  data:{
    password:'',
    repassword:'',
    matchit:false,
    showicon:false
  },
  watch:{
    repassword:function(){
      vm.showicon = true;
      if(vm.password == vm.repassword)
        vm.matchit = true;
    }
  }
})