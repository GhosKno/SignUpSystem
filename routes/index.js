var router = require('koa-router')();

router.get('/', async function (ctx, next) {
  ctx.redirect('./signin');//默认跳转登录界面
  await next();
})
module.exports = router;
