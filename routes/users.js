var router = require('koa-router')();

router.get('/', async function(ctx, next) {
  if (!ctx.session.user) //未登录，跳转到登录界面
    ctx.redirect('./signin');
  var Admuser = ctx.request.db_admusers;
  await Admuser.findOne({ username: ctx.session.user }, function(err, doc) {
    if (doc)
      ctx.session.admuser = ctx.session.user;
  })
  if (ctx.session.admuser)
    ctx.redirect('./admin');
  await ctx.render('users');
  await next();
})
router.get('/signout', async function(ctx, next) {
  ctx.session.user = ctx.session.admuser = null;
  ctx.redirect('../signin');
  await next();
})
router.get('/get_info', async function(ctx, next) { //获取用户界面所需的所有数据库信息
  if (!ctx.session.user) //未登录，返回错误信息
    ctx.body = '没有登录，快给我滚';
  var info = {
    tpls: {},
    user: ctx.session.user,
    his: [],
  };
  var Tpls = ctx.request.db_tpls;
  await Tpls.find({}, function(err, docs) {
      // var his_item = {};
      if (err)
        console.error(err);
      info.tpls = docs;
      info.tpls.forEach(function(el, index) {
        var his_item = el.datas.find(function(el) {
          return el.user = ctx.session.user;
        });
        if (his_item) {
          his_item.name = el.name;
          his_item.des = el.des;
          info.his.push(his_item);
        }
      })
    })
    .sort({ 'name': -1 })
    .then()
    .catch(function(e) { console.error(e) });
  info.tpls.forEach(function(el, index, info) {
    info[index].datas = null;
  })
  ctx.body = info;
})
router.post('/sub_info', async function(ctx, next) {
  var Tpl = ctx.request.db_tpls;
  // await Tpl.remove({ name: ctx.request.body.tpl_name, datas[user]: })
  var datas = [];
  await Tpl.findOne({ name: ctx.request.body.tpl_name }, function(err, doc) {
    if (err)
      console.error(err);
    else if (doc) {
      // console.log(doc);
      datas = doc.datas;
    }
  });
  var update_type = false;
  if (datas.length > 0) {
    datas.forEach(function(el, index, datas) {
      if (el.user == ctx.session.user) {
        datas[index] = ctx.request.body.data;
        update_type = true;
      }
    })
  }
  // console.log(datas,update_type);
  if (!update_type)
    await Tpl.update({ name: ctx.request.body.tpl_name }, { $push: { 'datas': ctx.request.body.data }, $inc: { 'count': 1 } })
    .then(function() {

      ctx.body = 'success';
    }).catch(function(e) {
      console.error(e);
    });
  else {
    await Tpl.update({ name: ctx.request.body.tpl_name }, { $set: { 'datas': datas } })
      .then(function() {
        ctx.body = 'success';
      }).catch(function(e) {
        console.error(e);
      });
  }
})
module.exports = router;
