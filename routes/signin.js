var router = require('koa-router')();
var sha1 = require('sha1');

router.get('/', async function(ctx, next) {
  if (ctx.session.user) //已登录，跳转到用户界面
    ctx.redirect('./users');
  await ctx.render('signin', { username: '', errmsg: '' });
  await next();
})
router.post('/', async function(ctx, next) {
  var User = ctx.request.db_comusers; //数据表Model
  // console.log(User);
  // var user = new User({  //实例
  //   username:ctx.request.body.username,
  //   password:ctx.request.body.password
  // });
  var user = {
    username: ctx.request.body.username,
    password: ctx.request.body.password
  };
  console.log(user);

  var userexec = false;
  await User.findOne({
    username: ctx.request.body.username,
    password: sha1(ctx.request.body.password)
  }, function(err, doc) {
    if (err)
      console.error(err);
    if (doc) {
      ctx.session.user = user.username;
      userexec = true;
    }
  })
  if (userexec)
    ctx.redirect('./users');
  else {
    ctx.body = 'error';
    await ctx.render('signin', { username: ctx.request.body.username, remsg: '用户名或密码错误！' });

  }
  await next();

})
module.exports = router;
