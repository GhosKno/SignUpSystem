var router = require('koa-router')();
var sha1 = require('sha1');

router.get('/', async function(ctx, next) {
  if (!ctx.session.admuser) //未登录，跳转到登录界面
    ctx.redirect('./signin');
  await ctx.render('admin');
  await next();
})
router.get('/get_info', async function(ctx, next) { //获取管理员用户所需的所有信息
  if (!ctx.session.admuser) //未登录，返回错误信息
    ctx.body = '没有权限，快给我滚';
  var info = {
    comusers: {},
    tpls: {},
    logs: {}
  };
  var db_list = [ctx.request.db_comusers, ctx.request.db_tpls, ctx.request.db_logs];
  var db_index = 0;
  for (var value in info) {
    console.log(value);
    await db_list[db_index].find({}, function(err, docs) {
        if (err)
          console.error(err);
        info[value] = docs;
      })
      .sort({ 'name': -1, 'username': -1 })
      .then()
      .catch(function(e) { cosnole.error(e) });
    db_index++;
  }
  info.user = ctx.session.admuser;
  ctx.body = info;
})
router.post('/add_tpl', async function(ctx, next) {
  var Tpl = ctx.request.db_tpls;
  console.log(ctx.request.body);
  if (ctx.request.body._id) {
    await Tpl.remove({ _id: ctx.request.body._id });
  }
  var tpl = new Tpl(ctx.request.body);
  var tplexec = false;
  await Tpl.findOne({
    name: tpl.name
  }, function(err, doc) {
    if (err)
      console.error(err);
    if (doc) {
      tplexec = true;
    }
  })
  if (!tplexec)
    await tpl.save(function(err, res) {
      if (err)
        console.error(err);
      else {
        ctx.body = 'success';
        // console.log(res);
      }
    })
  else {
    ctx.body = '活动名重复';
  }
})
router.post('/dele_tpl', async function(ctx, next) {
  var Tpl = ctx.request.db_tpls;
  console.log(ctx.request.body);
  await Tpl.remove(ctx.request.body).then(function(res) {
    if (res.result.ok && res.result.n > 0)
      ctx.body = 'success';
    else
      ctx.body = 'error';
  });
  console.log(ctx.body);
})
router.post('/add_log', async function(ctx, next) {
  var Log = ctx.request.db_logs;
  var log = new Log(ctx.request.body);
  await log.save(function(err, res) {
    if (err)
      console.error(err);
    else {
      ctx.body = 'success';
      // console.log(res);
    }
  })
})
router.post('/add_user', async function(ctx, next) {
  var User = ctx.request.db_comusers;
  var user = new User({
    username: ctx.request.body.username,
    password: sha1(ctx.request.body.password)
  });
  await user.save(function(err, res) {
    if (err)
      console.error(err);
    else {
      ctx.body = 'success';
      // console.log(res);
    }
  })
})
router.post('/dele_user', async function(ctx, next) {
  var User = ctx.request.db_comusers;
  console.log(ctx.request.body);
  await User.remove(ctx.request.body).then(function(res) {
    if (res.result.ok && res.result.n > 0)
      ctx.body = 'success';
    else
      ctx.body = 'error';
  });
  console.log(ctx.body);
})
router.post('/dele_log', async function(ctx, next) {
  var Log = ctx.request.db_logs;
  console.log(ctx.request.body);
  await Log.remove(ctx.request.body).then(function(res) {
    if (res.result.ok && res.result.n > 0)
      ctx.body = 'success';
    else
      ctx.body = 'error';
  });
  console.log(ctx.body);
})
router.get('/signout', async function(ctx, next) {
  ctx.session.user = ctx.session.admuser = null;
  ctx.redirect('../signin');
  await next();
})
module.exports = router;
