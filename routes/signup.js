var router = require('koa-router')();
var sha1 = require('sha1');

router.get('/', async function(ctx, next) {
  if (ctx.session.user) //已登录，跳转到用户界面
    ctx.redirect('./users');
  await ctx.render('signup', { username: '', errmsg: '' });
  await next();
})
router.post('/', async function(ctx, next) {
  var User = ctx.request.db_comusers; //数据表Model
  // console.log(User);
  var user = new User({ //实例
    username: ctx.request.body.username,
    password: sha1(ctx.request.body.password)
  });
  console.log(user);

  var userexec = false;
  await User.findOne({
    username: ctx.request.body.username
  }, function(err, doc) {
    if (err)
      console.error(err);
    if (doc) {
      userexec = true;
    }
  })
  if (!userexec)
    await user.save(function(err, res) {
      if (err)
        console.error(err);
      else {
        console.log(res);
        ctx.session.user = user.username;
        ctx.redirect('./users');
      }
    })
  else {
    ctx.body = 'error';
    await ctx.render('signup', { username: ctx.request.body.username, remsg: '用户名已存在！' });
  }
  await next();

})
module.exports = router;
