# SignUpSystem
## 可自定义模板的信息收集应用

## 功能
  - admin用户
    - 发布/修改/删除模板
    - 添加/删除用户
    - 查看采集信息
    - 查看/删除日志
  - 普通user用户
    - 填写信息
    - 查看个人历史记录

## 使用
1. 全局安装koa-generator：
  ``` sudo npm install koa-generator -g```
2. 开启MongoDB服务(已安装MongoDB)：`` mongod ./data``
3. 进入项目目录，运行`` npm start``，浏览器打开本地3000端口

## 说明
**简单易上手，~~不用说明~~**