const Koa = require('koa');
const app = new Koa();
const router = require('koa-router')();
const views = require('koa-views');
const co = require('co');
const convert = require('koa-convert');
const json = require('koa-json');
const onerror = require('koa-onerror');
const session = require('koa-session');
const bodyparser = require('koa-bodyparser')();
const logger = require('koa-logger');
app.keys = ['ghoskno koa-pro SUS'];
app.use(session(app));

var mongo = require('mongodb');
var mongoose = require('mongoose');
var dbmodel = require('./model/dbmodel');

const index = require('./routes/index');
const users = require('./routes/users');
const admin = require('./routes/admin');
const signin = require('./routes/signin');
const signup = require('./routes/signup');

// middlewares
app.use(convert(bodyparser));
app.use(convert(json()));
app.use(convert(logger()));
app.use(require('koa-static')(__dirname + '/public'));

app.use(views(__dirname + '/views', {
  extension: 'jade'
}));

// db setting
app.use(async (ctx,next) => {
  ctx.request.db_comusers = mongoose.model('comuser',dbmodel.comusers);
  ctx.request.db_admusers = mongoose.model('admuser',dbmodel.admusers);
  ctx.request.db_logs = mongoose.model('log',dbmodel.logs);
  ctx.request.db_tpls = mongoose.model('tpl',dbmodel.tpls);
  await next();
});

// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

router.use('/', index.routes(), index.allowedMethods());
router.use('/admin', admin.routes(), admin.allowedMethods());
router.use('/users', users.routes(), users.allowedMethods());
router.use('/signin', signin.routes(), signin.allowedMethods());
router.use('/signup', signup.routes(), signup.allowedMethods());

app.use(router.routes(), router.allowedMethods());
// response

app.on('error', function(err, ctx){
  console.log(err)
  logger.error('server error', err, ctx);
});


module.exports = app;