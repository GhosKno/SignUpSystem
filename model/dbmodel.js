  var mongoose = require('mongoose');
  var db = mongoose.connect('mongodb://@localhost:27017/SUS');

  var comusers = new mongoose.Schema({
    username: { // 真实姓名
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    }
  });
  var admusers = new mongoose.Schema({
    username: {
      type: String,
      required: true
    }
  });
  var logs = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    date: {
      type: String,
      required: true
    },
    des: {
      type: String,
      required: true
    },
    user: {
      type: String,
      required: true
    }
  });
  var tpls = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    des: {
      type: String,
      required: true
    },
    count: {
      type: Number,
      required: true
    },
    inputs: {
      type: Array,
      required: false
    },
    datas: {
      type: Array,
      required: false
    },
    clue: {
      type: Array,
      required: false
    }
  });
  exports.comusers = comusers;
  exports.admusers = admusers;
  exports.tpls = tpls;
  exports.logs = logs;
